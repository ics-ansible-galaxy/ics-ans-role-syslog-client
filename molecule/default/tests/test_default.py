import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_config_file_exists(host):
    file = host.file('/etc/syslog-ng/syslog-ng.conf')
    assert file.exists
    assert file.mode == 0644
    assert file.user == 'root'


def test_syslog_enabled_and_running(host):
    service = host.service('syslog-ng.service')
    assert service.is_running
    assert service.is_enabled
