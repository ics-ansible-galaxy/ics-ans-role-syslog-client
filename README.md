# ics-ans-role-syslog-client

Ansible role to install syslog-client.

## Requirements

- ansible >= 2.7
- molecule >= 2.6

## Role Variables

```yaml
syslog_destination_ip: hostname
syslog_destination_port: port
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-syslog-client
```

## License

BSD 2-clause
---
